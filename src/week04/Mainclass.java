package week04;

import java.util.Scanner;

/**
 * Created by marti on 30.11.2016.
 */
public class Mainclass {
    public static void main(String[] args) {
        BottleDispenser bd = new BottleDispenser();
        Scanner sc = new Scanner(System.in);
        int choice = -1;

        while (choice != 0) {
            System.out.println();
            System.out.println("*** LIMSA-AUTOMAATTI ***");
            System.out.println("1) Lisää rahaa koneeseen");
            System.out.println("2) Osta pullo");
            System.out.println("3) Ota rahat ulos");
            System.out.println("4) Listaa koneessa olevat pullot");
            System.out.println("0) Lopeta");
            System.out.print("Valintasi: ");
            choice = sc.nextInt();

            switch (choice) {
                case 1:     bd.addMoney();
                            break;
                case 2:     bd.listBottles();
                            System.out.print("Valintasi: ");
                            int bottleNumber = sc.nextInt();
                            bd.buyBottle(bottleNumber);
                            break;
                case 3:     bd.returnMoney();
                            break;
                case 4:     bd.listBottles();
                            break;
                default:    break;
            }

        }
    }
}
