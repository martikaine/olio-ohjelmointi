package week04;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by marti on 30.11.2016.
 */
public class BottleDispenser {

    private int bottles;
    private ArrayList<Bottle> bottle_array;
    private double money;

    public BottleDispenser() {
        bottles = 6;
        money = 0;

        // Initialize the array
        bottle_array = new ArrayList<>(bottles);
        // Add Bottle-objects to the array
        bottle_array.add(new Bottle("Pepsi Max", 0.5, 1.8));
        bottle_array.add(new Bottle("Pepsi Max", 1.5, 2.2));
        bottle_array.add(new Bottle("Coca-Cola Zero", 0.5, 2.0));
        bottle_array.add(new Bottle("Coca-Cola Zero", 1.5, 2.5));
        bottle_array.add(new Bottle("Fanta Zero", 0.5, 1.95));
        bottle_array.add(new Bottle("Fanta Zero", 0.5, 1.95));
    }

    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }

    public void buyBottle(int choice) {
        if (bottles == 0) {
            System.out.println("Pullot loppu!");
            return;
        }

        if (choice > 0 && choice <= bottle_array.size()) {
            Bottle bottle = bottle_array.get(choice - 1);

            if (money < bottle.getPrice()) {
                System.out.println("Syötä rahaa ensin!");
            } else {
                bottles -= 1;
                money -= bottle.getPrice();

                System.out.println("KACHUNK! " + bottle.getName() + " tipahti masiinasta!");
                deleteBottle(choice - 1);
            }
        } else {
            System.out.println("Ei listalla!");
        }
    }

    public void returnMoney() {
        DecimalFormat df = new DecimalFormat("#0.00");
        System.out.println("Klink klink. Sinne menivät rahat! Rahaa tuli ulos " + df.format(money) + "€");
        money = 0;
    }

    public void listBottles() {
        for (int i = 0; i < bottle_array.size(); i++) {
            Bottle bottle = bottle_array.get(i);
            System.out.println(i+1 + ". Nimi: " + bottle.getName());
            System.out.println("\tKoko: " + bottle.getSize() + "\tHinta: " + bottle.getPrice());
        }
    }

    private void deleteBottle(int index) {
        bottle_array.remove(index);
    }
}