package week04;

/**
 * Created by marti on 30.11.2016.
 */
public class Bottle {
    private String name;
    //private String manufacturer;
    //private double totalEnergy;
    private double size;
    private double price;

    public Bottle() {
        name = "Pepsi Max";
        //manufacturer = "Pepsi";
        //totalEnergy = 0.3;
        size = 0.5;
        price = 1.80;
    }

    public Bottle(String name, double size, double price) {
        this.name = name;
        this.size = size;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    /*
    public String getManufacturer() {
        return manufacturer;
    }

    public double getTotalEnergy() {
        return totalEnergy;
    }
    */

    public double getSize() {
        return size;
    }

    public double getPrice() {
        return price;
    }
}
