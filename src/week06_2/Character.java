package week06_2;


public class Character {
    public WeaponBehavior weapon;
    public String className;

    public Character(WeaponBehavior weapon, String className) {
        this.weapon = weapon;
        this.className = className;
    }

    public void fight() {
        System.out.println(className + weapon.useWeapon());
    }
}

class King extends Character {
    public King(WeaponBehavior weapon) {
        super(weapon, "King");
    }
}

class Queen extends Character {
    public Queen(WeaponBehavior weapon) {
        super(weapon, "Queen");
    }
}

class Knight extends Character {
    public Knight(WeaponBehavior weapon) {
        super(weapon, "Knight");
    }
}

class Troll extends Character {
    public Troll(WeaponBehavior weapon) {
        super(weapon, "Troll");
    }
}

class WeaponBehavior {
    private String name;

    public WeaponBehavior(String name) {
        this.name = name;
    }

    public String useWeapon() {
        return " tappelee aseella " + name;
    }
}

class KnifeBehavior extends WeaponBehavior {
    public KnifeBehavior () {
        super("Knife");
    }
}

class AxeBehavior extends WeaponBehavior {
    public AxeBehavior () {
        super("Axe");
    }
}

class SwordBehavior extends WeaponBehavior {
    public SwordBehavior () {
        super("Sword");
    }
}

class ClubBehavior extends WeaponBehavior {
    public ClubBehavior () {
        super("Club");
    }
}


