package week06_2;

import java.util.Scanner;

public class Mainclass {
    public static void main(String args[]) {
        new App();
    }
}

class App {
    Character currentCharacter;
    Scanner sc = new Scanner(System.in);
    boolean shouldQuit = false;

    App() {
        while (!shouldQuit) {
            shouldQuit = mainMenu();
        }
    }

    private int getChoice() {
        int choice;
        System.out.print("Valintasi: ");
        choice = sc.nextInt();
        return choice;
    }

    private boolean mainMenu() {
        int choice;

        System.out.println("*** TAISTELUSIMULAATTORI ***");
        System.out.println("1) Luo hahmo");
        System.out.println("2) Taistele hahmolla");
        System.out.println("0) Lopeta");
        do {
            choice = getChoice();
        } while (choice < 0 || choice > 2);

        switch(choice) {
            case 1:
                addCharacterMenu();
                break;
            case 2:
                fight();
                break;
            case 0:
                return true;
            default:
                break;
        }
        return false;
    }

    private void addCharacterMenu() {
        int choice;

        System.out.println("Valitse hahmosi: ");
        System.out.println("1) Kuningas");
        System.out.println("2) Ritari");
        System.out.println("3) Kuningatar");
        System.out.println("4) Peikko");

        do {
            choice = getChoice();
        } while (choice < 1 || choice > 4);

        WeaponBehavior w = selectWeaponMenu();

        switch(choice) {
            case 1: currentCharacter = new King(w);
                break;
            case 2: currentCharacter = new Knight(w);
                break;
            case 3: currentCharacter = new Queen(w);
                break;
            case 4: currentCharacter = new Troll(w);
                break;
            default:
                break;
        }
    }

    private WeaponBehavior selectWeaponMenu() {
        int choice;

        System.out.println("Valitse aseesi: ");
        System.out.println("1) Veitsi");
        System.out.println("2) Kirves");
        System.out.println("3) Miekka");
        System.out.println("4) Nuija");
        do {
            choice = getChoice();
        } while (choice < 1 || choice > 4);

        switch (choice) {
            case 1:
                return new KnifeBehavior();
            case 2:
                return new AxeBehavior();
            case 3:
                return new SwordBehavior();
            case 4:
                return new ClubBehavior();
            default:
                return new KnifeBehavior();
        }
    }

    private void fight() {
        if (currentCharacter != null) currentCharacter.fight();
    }
}
