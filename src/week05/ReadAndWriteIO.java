package week05;

import java.io.*;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ReadAndWriteIO {
    private BufferedReader in;
    private BufferedWriter out;
    private String line;

    public void readFile(String fileName) throws IOException {
        in = new BufferedReader(new FileReader(fileName));
        while ((line = in.readLine()) != null) {
            System.out.println(line);
        }

        in.close();
    }

    public void readFromZipFile(String fileName) throws IOException {
        ZipFile zip = new ZipFile(fileName);
        Enumeration entries = zip.entries();
        ZipEntry inputFile = (ZipEntry) entries.nextElement();
        in = new BufferedReader(new InputStreamReader(zip.getInputStream(inputFile)));

        while ((line = in.readLine()) != null) {
            System.out.println(line);
        }

        in.close();
        zip.close();
    }

    public void readAndWrite(String inputFile, String outputFile) throws IOException {
        in = new BufferedReader(new FileReader(inputFile));
        out = new BufferedWriter(new FileWriter(outputFile));

        while ((line = in.readLine()) != null) {
            out.write(line);
        }

        in.close();
        out.close();
    }

    // Read file and write non-empty lines under 30 characters long to another file
    public void readAndWriteUnder30(String inputFile, String outputFile) throws IOException {
        in = new BufferedReader(new FileReader(inputFile));
        out = new BufferedWriter(new FileWriter(outputFile));

        while ((line = in.readLine()) != null) {
            if (line.length() < 30 && !line.trim().isEmpty()) {
                out.write(line);
                out.newLine();
            }
        }

        in.close();
        out.close();
    }

    public void readAndWriteContainingChar(String inputFile, String outputFile, CharSequence charSequence) throws IOException {
        in = new BufferedReader(new FileReader(inputFile));
        out = new BufferedWriter(new FileWriter(outputFile));

        while ((line = in.readLine()) != null) {
            if (line.length() < 30 && !line.trim().isEmpty() && line.contains(charSequence)) {
                out.write(line);
                out.newLine();
            }
        }

        in.close();
        out.close();
    }
}