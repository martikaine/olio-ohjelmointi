package week05;

import java.io.IOException;

public class Mainclass {
    public static void main(String[] args) throws IOException {
        String path = System.getProperty( "user.dir" );
        System.out.println(path);
        ReadAndWriteIO rw = new ReadAndWriteIO();
        // rw.readFile();

        rw.readAndWrite(path + "/input.txt", path + "/output.txt");
    }
}