package week07;

public abstract class Account {
    protected int money;
    protected String accountID;

    Account(String accountID, int money) {
        this.money = money;
        this.accountID = accountID;
        System.out.println("Tili luotu.");
    }

    public String getAccountID() {
        return accountID;
    }

    public abstract void printDetails();
    public abstract void changeMoneyAmount(int amount);
}

class BasicAccount extends Account {
    BasicAccount(String accountID, int money) {
        super(accountID, money);
    }

    public void changeMoneyAmount(int amount) {
        if (amount + money >= 0) {
            money += amount;
        } else {
            System.out.println("Tilillä ei ole tarpeeksi rahaa.");
        }
    }

    public void printDetails() {
        System.out.println("Tilinumero: " + accountID + " Tilillä rahaa: " + money);
    }
}

class CreditAccount extends Account {
    private int credit;

    CreditAccount(String accountID, int money, int credit) {
        super(accountID, money);
        this.credit = credit;
    }

    public void printDetails() {
        System.out.println("Tilinumero: " + accountID + " Tilillä rahaa: " + money + " Luottoraja: " + credit);
    }

    public void changeMoneyAmount(int amount) {
        if (amount + money + credit >= 0) {
            money += amount;
        } else {
            System.out.println("Tilillä ei ole tarpeeksi rahaa.");
        }
    }
}