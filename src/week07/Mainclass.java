package week07;

import java.util.Scanner;

public class Mainclass {
    public static void main(String args[]) {
        new BankingApp();
    }
}

class BankingApp {
    private Bank bank;
    private Scanner sc;

    BankingApp() {
        bank = new Bank();
        sc = new Scanner(System.in);
        int userChoice;

        do {
            printMenu();
            userChoice = getChoice();
            handleChoice(userChoice);
        } while (userChoice != 0);
    }

    private void printMenu() {
        System.out.println();
        System.out.println("*** PANKKIJÄRJESTELMÄ ***");
        System.out.println("1) Lisää tavallinen tili");
        System.out.println("2) Lisää luotollinen tili");
        System.out.println("3) Tallenna tilille rahaa");
        System.out.println("4) Nosta tililtä");
        System.out.println("5) Poista tili");
        System.out.println("6) Tulosta tili");
        System.out.println("7) Tulosta kaikki tilit");
        System.out.println("0) Lopeta");
    }

    private int getChoice() {
        System.out.print("Valintasi: ");
        return sc.nextInt();
    }

    private void handleChoice(int choice) {
        String id;
        int money;
        int credit;

        switch(choice) {
            case 1:
                System.out.print("Syötä tilinumero: ");
                id = sc.next();
                System.out.print("Syötä rahamäärä: ");
                money = sc.nextInt();

                bank.addBasicAccount(id, money);
                break;
            case 2:
                System.out.print("Syötä tilinumero: ");
                id = sc.next();
                System.out.print("Syötä rahamäärä: ");
                money = sc.nextInt();
                System.out.print("Syötä luottoraja: ");
                credit = sc.nextInt();

                bank.addCreditAccount(id, money, credit);
                break;
            case 3:
                System.out.print("Syötä tilinumero: ");
                id = sc.next();
                System.out.print("Syötä rahamäärä: ");
                money = sc.nextInt();
                if (isNegative(money)) return;

                bank.changeAccountMoneyAmount(id, money);
                break;
            case 4:
                System.out.print("Syötä tilinumero: ");
                id = sc.next();
                System.out.print("Syötä rahamäärä: ");
                money = sc.nextInt();
                if (isNegative(money)) return;

                bank.changeAccountMoneyAmount(id, -money);
                break;
            case 5:
                System.out.print("Syötä poistettava tilinumero: ");
                id = sc.next();

                bank.removeAccount(id);
                break;
            case 6:
                System.out.print("Syötä tulostettava tilinumero: ");
                id = sc.next();

                bank.printSingleAccount(id);
                break;
            case 7:
                bank.printAllAccounts();
                break;
            case 0:
                break;
            default:
                System.out.println("Valinta ei kelpaa.");
                break;
        }
    }

    private boolean isNegative(int number) {
        if (number < 0) {
            System.out.println("Vain positiiviset luvut kelpaavat.");
            return true;
        }
        return false;
    }
}



