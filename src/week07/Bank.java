package week07;

import java.util.ArrayList;

public class Bank {
    private ArrayList<Account> accounts = new ArrayList<>();

    public void addBasicAccount(String accountID, int money) {
        BasicAccount a = new BasicAccount(accountID, money);
        accounts.add(a);
    }

    public void addCreditAccount(String accountID, int money, int credit) {
        CreditAccount a = new CreditAccount(accountID, money, credit);
        accounts.add(a);
    }

    // returns account's index in the ArrayList or -1 if it doesn't exist
    private int findAccountIndex(String accountID) {
        for (int i = 0; i < accounts.size(); i++) {
            if (accountID.equals(accounts.get(i).getAccountID())) return i;
        }
        System.out.println("Tiliä ei löytynyt.");
        return -1;
    }

    public void changeAccountMoneyAmount(String accountID, int amount) {
        int index;
        if ((index = findAccountIndex(accountID)) > -1) {
            accounts.get(index).changeMoneyAmount(amount);
        }
    }

    public void removeAccount(String accountID) {
        int index;
        if ((index = findAccountIndex(accountID)) > -1) {
            accounts.remove(index);
            System.out.println("Tili poistettu.");
        }
    }

    public void printSingleAccount(String accountID) {
        int index;
        if ((index = findAccountIndex(accountID)) > -1) {
            accounts.get(index).printDetails();
        }
    }

    public void printAllAccounts() {
        System.out.println("Kaikki tilit:");
        for (Account a : accounts) {
            a.printDetails();
        }
    }
}