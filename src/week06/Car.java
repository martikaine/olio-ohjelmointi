package week06;

import java.util.ArrayList;

public class Car {
    private ArrayList<CarPart> parts = new ArrayList<>();
    private ArrayList<Wheel> wheels = new ArrayList<>();

    public Car() {
        parts.add(new Engine());
        parts.add(new Body());
        parts.add(new Chassis());
        for (int i = 0; i < 4; i++) {
            wheels.add(new Wheel());
        }
    }

    public void print() {
        System.out.println("Autoon kuuluu:");
        for (CarPart part : parts) {
            System.out.println("\t" + part.getName());
        }
        System.out.println("\t" + wheels.size() + " " + wheels.get(0).getName());
    }
}

class CarPart {
    private String name;

    public CarPart() {
        name = this.getClass().getSimpleName();
        System.out.println("Valmistetaan: " + name);
    }

    public String getName() {
        return name;
    }
}

class Engine extends CarPart {
}

class Body extends CarPart {
}

class Chassis extends CarPart {
}

class Wheel extends CarPart {
}