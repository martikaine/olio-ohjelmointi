package week03;

import java.util.Scanner;

/**
 * Created by Matti on 18.9.2016.
 */
public class Dog {
    private String myName;

    public Dog(String name) {
        if (!name.trim().isEmpty()) {
            myName = name;
        } else {
            myName = "Doge";
        }
        System.out.println("Hei, nimeni on " + myName + "!");
    }

    public void speak(String phrase) {

        System.out.print(myName + ": ");
        Scanner sc = new Scanner(phrase);
        while (sc.hasNext()) {
            if (sc.hasNextBoolean()) {
                System.out.print("bool: ");
            } else if (sc.hasNextInt()) {
                System.out.print("int: ");
            }
            System.out.print(sc.next());
        }
    }
}
