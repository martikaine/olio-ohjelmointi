package week03;

import java.util.Scanner;

/**
 * Created by Matti on 18.9.2016.
 */
public class Mainclass {
    public static void main(String[] args) {
        String name, phrase;
        Scanner scan = new Scanner(System.in);

        System.out.print("Anna koiralle nimi: ");
        name = scan.nextLine();
        Dog d = new Dog(name);

        System.out.print("Mitä koira sanoo: ");
        phrase = scan.nextLine();
        d.speak(phrase);
    }
}
